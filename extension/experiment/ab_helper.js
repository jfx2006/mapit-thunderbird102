/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

// Get various parts of the WebExtension framework that we need.
/* global ChromeUtils:false */

// noinspection ES6ConvertVarToLetConst
var { ExtensionCommon } = ChromeUtils.import("resource://gre/modules/ExtensionCommon.jsm")
// noinspection ES6ConvertVarToLetConst
var { VCardProperties } = ChromeUtils.import("resource:///modules/VCardUtils.jsm")

let abHelperManager

class ABHelperManager extends ExtensionCommon.EventEmitter {
  constructor(extension) {
    super()
    this.extension = extension
    this.window = null
    this.cardsPane = null
    this.detailsPane = null
  }
  waitForABReady(abBrowser) {
    this.window = abBrowser.contentWindow

    let global = abBrowser.ownerGlobal
    if (this.window.document.readyState !== "complete") {
      // console.log(this.window.document.readyState)
      let complete = false
      for (let i = 1; i < 6; i++) {
        global.setTimeout(() => {
          // console.log(`setTimeout ${i}: ${this.window.document.readyState}`)
          if (!complete && this.window.document.readyState === "complete") {
            // console.log(`setTimeout ${i}: ${this.window.document.readyState}, continuing`)
            complete = true
            abHelperManager.setAB()
          } else if (!complete && i === 5) {
            console.log(`setTimeout ${i}: ${this.window.document.readyState}, aborting`)
          }
        }, i * 1000)
      }
    } else {
      // console.log(`cardsPane already set! ${this.window.document.readyState}`)
      abHelperManager.setAB()
    }
  }
  setAB() {
    if (this.window.cardsPane) {
      this.cardsPane = this.window.cardsPane
      this.detailsPane = this.window.detailsPane
      this.cardsPane.cardsList.addEventListener("select", this.handleEvent)
      abHelperManager.emit("ab-loaded")
    } else {
      console.log("cardsPane not set!")
    }
  }
  addMapItButtons(buttons) {
    const maybe_buttons = this.detailsPane.node.querySelectorAll(".mapit-button")
    for (const btn of maybe_buttons) {
      btn.remove()
    }
    let addressEntries = this.detailsPane.node.querySelectorAll(
      "section#addresses > .entry-list > .entry-item > span.entry-value",
    )
    let btn_elems = buttons.map((b) => this._createMapItButton(b.label, b.tooltip, b.href))
    if (addressEntries.length !== btn_elems.length) {
      console.log(
        // eslint-disable-next-line max-len
        `Something went wrong: addressEntries.length !== btn_elems.length. ${addressEntries.length} !== ${btn_elems.length}`,
      )
      return
    }
    for (let i = 0; i < addressEntries.length; i++) {
      addressEntries[i].appendChild(this.window.document.createElement("br"))
      addressEntries[i].appendChild(btn_elems[i])
    }
  }
  refreshMapItButtons(cardUID) {
    // The currently displayed card was updated somehow. This tends to
    // cause the Map buttons to disappear, so put them back probably
    const displayedCard = this.detailsPane.currentCard
    if (cardUID !== displayedCard.UID) {
      console.log(`Got updated card ${cardUID} but displaying ${displayedCard.UID}!`)
      return
    }
    abHelperManager._getButtons(displayedCard)
  }
  _createMapItButton(label, tooltip, href) {
    const button = this.window.document.createElement("button")
    button.className = "mapit-button"
    button.innerText = label
    button.title = tooltip
    button.dataset["href"] = href
    button.onclick = function (e) {
      const href = e.target.dataset["href"]
      abHelperManager.emit("mapit-clicked", href)
    }
    return button
  }
  handleEvent(event) {
    switch (event.type) {
      case "select":
        abHelperManager._onSelect(event)
        break
    }
  }
  // noinspection JSUnusedLocalSymbols
  _onSelect(event) {
    let cards = this.cardsPane.selectedCards
    if (cards.length === 1) {
      let card = cards[0]
      abHelperManager._getButtons(card)
    }
  }
  _getButtons(card) {
    let vCardProperties = card.supportsVCard
      ? card.vCardProperties
      : VCardProperties.fromPropertyMap(new Map(card.properties.map((p) => [p.name, p.value])))
    let cardAddresses = []
    for (let entry of vCardProperties.getAllEntries("adr")) {
      cardAddresses.push(abHelperManager._entry2Object(entry))
    }
    const cardUID = card.UID
    abHelperManager.emit("card-selected", cardAddresses, cardUID)
  }
  _entry2Object(entry) {
    return {
      street: Array.isArray(entry.value[2]) ? entry.value[2][0] : entry.value[2],
      city: entry.value[3],
      state: entry.value[4],
      postalcode: entry.value[5],
      country: entry.value[6],
    }
  }
  add(name, callback) {
    switch (name) {
      case "onCardSelected":
        this.on("card-selected", callback)
        break
      case "onMapItClicked":
        this.on("mapit-clicked", callback)
        break
      case "onABLoaded":
        this.on("ab-loaded", callback)
        break
    }
  }
  remove(name, callback) {
    switch (name) {
      case "onCardSelected":
        this.off("card-selected", callback)
        if (this.cardsPane) {
          this.cardsPane.cardsList.removeEventListener("select", this.handleEvent)
        }
        break
      case "onMapItClicked":
        this.off("mapit-clicked", callback)
        break
      case "onABLoaded":
        this.off("ab-loaded", callback)
        break
    }
  }
}

// noinspection ES6ConvertVarToLetConst,JSUnusedGlobalSymbols
// eslint-disable-next-line no-unused-vars
var AB_helper = class extends ExtensionCommon.ExtensionAPI {
  constructor(extension) {
    super(extension)
    abHelperManager = new ABHelperManager(extension)
  }
  getAPI(context) {
    return {
      AB_helper: {
        async no_op() {
          console.log("no-op")
        },
        async inject(tab) {
          let tabObject = context.extension.tabManager.get(tab.id)
          let realTab = tabObject.nativeTab
          let abBrowser = realTab.panel.querySelector("browser")
          abHelperManager.waitForABReady(abBrowser)
        },
        async addMapItButtons(buttons) {
          abHelperManager.addMapItButtons(buttons)
        },
        async refresh(cardUID) {
          abHelperManager.refreshMapItButtons(cardUID)
        },
        onCardSelected: new ExtensionCommon.EventManager({
          context,
          name: "AB_helper.onCardSelected",
          // In this function we add listeners for any events we want to listen to, and return a
          // function that removes those listeners. To have the event fire in your extension,
          // call fire.async.
          register(fire) {
            const name = "onCardSelected"
            function callback(event, cardAddresses, cardUID) {
              return fire.async(cardAddresses, cardUID)
            }
            abHelperManager.add(name, callback)
            return function () {
              abHelperManager.remove(name, callback)
            }
          },
        }).api(),
        onMapItClicked: new ExtensionCommon.EventManager({
          context,
          name: "AB_helper.onMapItClicked",
          register(fire) {
            const name = "onMapItClicked"
            function callback(event, href) {
              return fire.async(href)
            }
            abHelperManager.add(name, callback)
            return function () {
              abHelperManager.remove(name, callback)
            }
          },
        }).api(),
        onABLoaded: new ExtensionCommon.EventManager({
          context,
          name: "AB_helper.onABLoaded",
          register(fire) {
            const name = "onABLoaded"
            // noinspection JSUnusedLocalSymbols
            function callback(event) {
              return fire.async()
            }

            abHelperManager.add(name, callback)
            return function () {
              abHelperManager.remove(name, callback)
            }
          },
        }).api(),
      },
    }
  }
}
