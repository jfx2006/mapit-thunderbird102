/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

"use strict"
/* global messenger:false */

import * as MapIt from "./mapit.js"

messenger.runtime.onInstalled.addListener(async (details) => {
  console.log(`onInstalled running... ${details.reason}`)
  const win = await messenger.windows.getCurrent()
  const winId = win.id

  async function installCallback() {
    await updateCallback()
    let onboardUrl = new URL(messenger.runtime.getURL("content/firstrun.html"))
    messenger.tabs.create({
      url: onboardUrl.href,
      windowId: winId,
    })
  }

  async function updateCallback() {
    let result = await messenger.storage.local.get("map_provider")
    if (!MapIt.MAP_PROVIDERS.includes(result.map_provider)) {
      messenger.storage.local.set({
        map_provider: MapIt.DEFAULT_MAP_PROVIDER,
      })
    }
  }

  switch (details.reason) {
    case "install":
      await installCallback()
      break
    case "update":
      await updateCallback()
      break
  }
})

async function handleTabCreated(tab) {
  if (tab.type !== "addressBook") {
    return
  }
  await messenger.AB_helper.inject(tab)
}

async function handleABLoaded(tab) {
  console.log(`ABLoaded: ${tab}`)
}

let currentCardUID = null
async function handleCardSelected(cardAddresses, cardUID) {
  currentCardUID = cardUID
  let buttons = []
  for (let a of cardAddresses) {
    let button = await MapIt.getMapItButton(a)
    buttons.push(button)
  }
  await messenger.AB_helper.addMapItButtons(buttons)
}

async function handleMapItClicked(href) {
  messenger.windows.openDefaultBrowser(href)
}

async function onUpdated(contact) {
  if (contact.id === currentCardUID) {
    // console.log(`Card ${currentCardUID} updated! Refreshing.`)
    // console.log(contact)
    await messenger.AB_helper.refresh(contact.id)
  }
}

messenger.AB_helper.onCardSelected.addListener(handleCardSelected)
messenger.AB_helper.onABLoaded.addListener(handleABLoaded)
messenger.tabs.onCreated.addListener(handleTabCreated)
messenger.AB_helper.onMapItClicked.addListener(handleMapItClicked)
messenger.contacts.onUpdated.addListener(onUpdated)

messenger.tabs.query({ type: "addressBook" }).then(async (tabs) => {
  for (const abTab of tabs) {
    await messenger.AB_helper.inject(abTab)
  }
})
