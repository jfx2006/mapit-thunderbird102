/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

/* global messenger:false */

export const MAP_PROVIDERS = ["openstreetmap", "google"]
export const DEFAULT_MAP_PROVIDER = "openstreetmap"

const URL_MAP = {
  openstreetmap: "provider_openstreetmap_url",
  google: "provider_google_maps_url",
}

async function createMapItURL(address) {
  const result = await messenger.storage.local.get("map_provider")
  let urlFormat = messenger.i18n.getMessage(URL_MAP[result.map_provider])
  if (!urlFormat) {
    throw `Error: urlFormat is null for ${address}`
  }

  urlFormat = urlFormat.replace("@street@", encodeURIComponent(address.street))
  urlFormat = urlFormat.replace("@city@", encodeURIComponent(address.city))
  urlFormat = urlFormat.replace("@state@", encodeURIComponent(address.state))
  urlFormat = urlFormat.replace("@postalcode@", encodeURIComponent(address.postalcode))
  urlFormat = urlFormat.replace("@country@", encodeURIComponent(address.country))

  let url = new URL(urlFormat)
  return url.href
}

function mapItButtonStrings() {
  let label = messenger.i18n.getMessage("button_mapit_label")
  let tooltip = messenger.i18n.getMessage("button_mapit_tooltip")

  return { label: label, tooltip: tooltip }
}

export async function getMapItButton(address) {
  let button = mapItButtonStrings()
  button["href"] = await createMapItURL(address)
  return button
}
