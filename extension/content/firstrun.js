/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

const frame = document.getElementById("options_embed")

frame.onload = function () {
  let height = frame.contentWindow.document.body.clientHeight + 16
  const width = frame.contentWindow.document.body.clientWidth + 16
  frame.style.height = `${height}px`
  frame.style.width = `${width}px`
}
