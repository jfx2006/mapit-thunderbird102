/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

"use strict"
/* global messenger:false */
;(async () => {
  function restoreOptions() {
    function setCurrentChoice(result) {
      switch (result.map_provider) {
        case "google":
          document.getElementById("map-provider-google_maps").checked = true
          break
        case "openstreetmap":
          document.getElementById("map-provider-openstreetmap").checked = true
          break
        default:
          onError(`Invalid provider found in storage: ${result}`)
      }
    }

    function onError(error) {
      console.log(`Error: ${error}`)
    }

    let getting = messenger.storage.local.get("map_provider")
    getting.then(setCurrentChoice, onError)
  }

  function saveOptions(event) {
    event.preventDefault()
    savedMessage.classList.add("visible")
    messenger.storage.local.set({
      map_provider: event.target.value,
    })
    setTimeout(function () {
      savedMessage.classList.remove("visible")
    }, 2000)
  }

  for (let elem of document.querySelectorAll("[name='map-provider']")) {
    elem.onchange = saveOptions
  }
  const savedMessage = document.getElementById("saved_message")

  function localizePage() {
    const page_prefix = ""
    const nodes = document.body.querySelectorAll("[data-i18n]")
    for (let n of nodes) {
      let message_id = `${page_prefix}${n.dataset.i18n}`
      let arg = n.dataset.i18nArg
      let message = messenger.i18n.getMessage(message_id, arg)
      if (message) {
        n.textContent = message
      }
    }
  }

  function onPageLoaded() {
    localizePage()
    restoreOptions()
  }

  document.addEventListener("DOMContentLoaded", onPageLoaded)
})()
