module.exports = {
  // Global options:
  verbose: true,
  // Command options:
  build: {
    overwriteDest: true,
    filename: "{name}-{version}.xpi",
  },
  sourceDir: "extension",
  ignoreFiles: ["**/test", "**/test/**"],
  run: {
    // must be in $PATH or a full path
    firefox: "/home/rob/bin/thunderbird128",
    // profile must exist or pass --profile-create-if-missing in the commandline
    // profile does not persist, use --keep-profile-changes to update it
    firefoxProfile: "/home/rob/.thunderbird/6hfgqn70.beta_mailext",
  },
}
