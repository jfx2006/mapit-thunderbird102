# ![icon](extension/icons/ab-maps-sm.svg) "Get Map" Button for Thunderbird 102

This is a mail extension to bring back the "Get Map" button for contact
addresses in the new Address Book in Thunderbird 102.

When clicked, the "Get Map" button will open the default web browser and display
a map of the associated contact's address.

![Screenshot](extension/content/getmap_screenshot.png)

OpenStreetMap and Google Maps are supported, defaulting to OpenStreetMap. The
setting can be changed in Thunderbird's Add-ons Manager.

### Extension Permissions & Mail Extension Experiments

This mail extension will request "full, unrestricted access to Thunderbird and
your computer". This is because mail extension "experiment" APIs are used to
add the button to when displaying a contact.

### Privacy & Data

"Get Map" for Thunderbird 102 does not collect or store any data aside from
your selected map provider.

OpenStreetMap and Google Maps have their own privacy policies. The address of
the contact will be sent encoded in the URL, along with other data as configured
in your default web browser.

### Supported Versions

The only supported versions are Thunderbird 102. Thunderbird 91 had this feature
built-in, and it's expected that Thunderbird 115 will include it as well.

