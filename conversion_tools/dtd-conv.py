#!/usr/bin/env python

import re
import json

LANGS = ("de", "en", "es-ES", "fr", "it", "ja", "nl", "pl", "pt-BR", "ru")

regex = re.compile(r'^.*"([^"]+)">$')

def get_strings(lang):
	rv = {"button_mapit_label": None,
			"button_mapit_tooltip": None
			}
	with open(f"/home/rob/tmp/{lang}/mapit.dtd") as fp:
		for line in fp.readlines():
			match = regex.match(line)
			if match:
				if "label" in line:
					rv["button_mapit_label"] = match.group(1)
				elif "tooltip" in line:
					rv["button_mapit_tooltip"] = match.group(1)
				else:
					raise Exception(f"wtf? lang was {lang} and line was {line}")
	
	if any([v is None for v in rv.values()]):
		raise Exception(f"wtf?? rv is {rv}")
	
	return rv

def add_to_messages(lang, lang_strings):
	addl = {}
	for ls in lang_strings:
		msg_key = ls
		msg_value = lang_strings[ls]
		addl[msg_key] = { "message": msg_value }
	
	msg_file = f"extension/_locales/{lang}/messages.json"
	with open(msg_file) as fp:
		msg_data = json.load(fp)
	
	msg_data.update(addl)
	
	with open(msg_file, "w") as fp:
		json.dump(msg_data, fp, indent=2, sort_keys=True, ensure_ascii=False)

for lang in LANGS:
	print(lang)
	lang_strings = get_strings(lang)
	add_to_messages(lang, lang_strings)

