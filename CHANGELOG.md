### 0.5

- Fix button colors on light themes
- Look for open address book tabs on install and inject helper

### 0.4

- Bump max supported version to 115.*

### 0.3

- Map Button didn't always appear, especially with remote address books
- Actually translate the Options page
